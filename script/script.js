let catsArr = [
  {
    id: 0001,
    catName: "Кот полосатый",
    catColor: "Коричневый окрас",
    catAge: "2",
    catPaws: "4",
    catPrice: "30 000",
    catInStock: true,
    catImage: "./src/cat1.png",
    catDiscount: "-40%",
    catLike: false,
  },
  {
    id: 0002,
    catName: "Кот полосатый",
    catColor: "Коричневый окрас",
    catAge: "6",
    catPaws: "4",
    catPrice: "40 000",
    catInStock: false,
    catImage: "./src/cat2.png",
    catDiscount: "0",
    catLike: false,
  },
  {
    id: 0003,
    catName: "Кот полосатый",
    catColor: "Коричневый окрас",
    catAge: "3",
    catPaws: "4",
    catPrice: "20 000",
    catInStock: true,
    catImage: "./src/cat3.png",
    catDiscount: "0",
    catLike: false,
  },
  {
    id: 0004,
    catName: "Кот полосатый",
    catColor: "Коричневый окрас",
    catAge: "7",
    catPaws: "4",
    catPrice: "25 000",
    catInStock: true,
    catImage: "./src/cat4.png",
    catDiscount: "0",
    catLike: false,
  },
  {
    id: 0005,
    catName: "Кот полосатый",
    catColor: "Коричневый окрас",
    catAge: "9",
    catPaws: "4",
    catPrice: "30 000",
    catInStock: true,
    catImage: "./src/cat5.png",
    catDiscount: "-40%",
    catLike: false,
  },
  {
    id: 0006,
    catName: "Кот полосатый",
    catColor: "Коричневый окрас",
    catAge: "12",
    catPaws: "4",
    catPrice: "10 000",
    catInStock: false,
    catImage: "./src/cat6.png",
    catDiscount: "0",
    catLike: true,
  },
];

let catsBlock = document.querySelector(".main__products-wrap");
let toTopButtom = document.querySelector(".onTopButton");
let sortOnPrice = document.querySelector(".main__sort_on-price");
let sortToAge = document.querySelector(".main__sort_to-age");
let email = document.querySelector(".footer__sub_email_input");
let submitBtn = document.querySelector(".footer__sub_submit");

let sortByPriceDirect = 1;
let sortByAgeDirect = 1;

function updateTheList() {
  catsBlock.innerHTML = "";
  catsArr.forEach((item) => {
    let discount = `
      <div class="main__cat_discount">
        <span class="main__cat_discount_text">${item.catDiscount}</span>
      </div>
      `;
    catsBlock.innerHTML += `
      <div class="main__cat-block">
        <div class="main__cat_foto">        
              ${item.catDiscount != "0" ? discount : ""}
          <img src="${item.catImage}" alt="cat" />
        </div>
        <img class="main__cat_like-img 
            ${item.catLike ? "main__cat_like" : "main__cat_no-like"}" 
            src="./src/like.svg" alt="like" 
          /> 
        <div class="main__cat_info">
          <div class="main__cat_info_title">${item.catName}</div>
          <div class="main__cat_info_block">
            <div class="main__cat_info_color">${item.catColor}</div>
            <div class="main__cat_info_age">
              <strong>${item.catAge} мес.</strong><br />Возраст
            </div>
            <div class="main__cat_info_paw">
              <strong>${item.catPaws}</strong><br />Кол-во лап
            </div>
          </div>
          <div class="main__cat_info_price">
            <strong>${item.catPrice} руб.</strong>
          </div>
        </div>
        <div class="main__cat_buy 
        ${item.catInStock ? "main__cat_sell" : "main__cat_sold-out"}">
          <span class="main__cat_buy_text">
          ${item.catInStock ? "Купить" : "Продан"}</span>
        </div>
      </div>
    `;
  });
  addEvents();
}
function sortByPrice() {
  sortByPriceDirect = sortByPriceDirect == 1 ? -1 : 1;
  catsArr.sort((a, b) => {
    a = a.catPrice.split(" ").join("");
    b = b.catPrice.split(" ").join("");
    if (+a > +b) {
      return sortByPriceDirect;
    } else if (+a < +b) {
      return sortByPriceDirect == 1 ? -1 : 1;
    }
    return 0;
  });
  updateTheList();
}
function sortByAge() {
  sortByAgeDirect = sortByAgeDirect == 1 ? -1 : 1;
  catsArr.sort((a, b) => {
    a = a.catAge.split(" ").join("");
    b = b.catAge.split(" ").join("");
    if (+a > +b) {
      return sortByAgeDirect;
    } else if (+a < +b) {
      return sortByAgeDirect == 1 ? -1 : 1;
    }
    return 0;
  });
  updateTheList();
}
function showToTopButton() {
  let x = window.pageYOffset;
  if (x > 300) {
    toTopButtom.classList.add("onTopButton_show");
  } else {
    toTopButtom.classList.remove("onTopButton_show");
  }
}
function toTopPage() {
  // window.scrollTo(0, 0);
  let el = document.querySelector(".header__wrap");
  el.scrollIntoView({ behavior: "smooth" });
}

function addEvents() {
  let likes = document.querySelectorAll(".main__cat_like-img");
  for (let i = 0; i < likes.length; i++) {
    likes[i].addEventListener("click", onLikeButton);
  }
}
function onLikeButton(event) {
  let targetCat = event.target;
  if (targetCat.classList.contains("main__cat_like")) {
    targetCat.classList.remove("main__cat_like");
  } else {
    targetCat.classList.add("main__cat_like");
    let wievBlock = document.querySelector(".main__cat_to-favorites");
    wievBlock.classList.add("main__cat_to-favorites_show");
    setTimeout(() => {
      wievBlock.classList.remove("main__cat_to-favorites_show");
    }, 1200);
  }
}

function validateEmail(value) {
  let regEX =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regEX.test(String(value).toLowerCase());
}
function onSubmitBtn(event) {
  let value = email.value;
  if (!validateEmail(value)) {
    event.preventDefault();
  }
}
function focusOutEmail(event) {
  if (!validateEmail(event.target.value)) {
    event.target.classList.add("footer__sub_email_input_wrong");
  } else {
    event.target.classList.remove("footer__sub_email_input_wrong");
  }
}

submitBtn.addEventListener("click", onSubmitBtn);
email.addEventListener("focusout", focusOutEmail);
window.addEventListener("scroll", showToTopButton);
toTopButtom.addEventListener("click", toTopPage);
sortOnPrice.addEventListener("click", sortByPrice);
sortToAge.addEventListener("click", sortByAge);
document.addEventListener("DOMContentLoaded", updateTheList);
